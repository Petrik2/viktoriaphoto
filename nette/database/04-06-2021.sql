-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `comments_ibfk_1` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `posts` (`id`, `title`, `content`, `created_at`, `user_id`) VALUES
(34,	'fr3f43r43',	'gfdsgfdsgfdsg',	'2020-10-07 18:34:20',	1),
(35,	'32432',	'4324',	'2020-10-07 18:38:38',	1),
(36,	'213',	'fr',	'2020-10-07 18:38:45',	1),
(37,	'hjdjfj',	'hhjfgfd\n',	'2020-10-07 18:58:41',	1),
(38,	'fdsf',	'dsfds',	'2020-10-07 18:59:20',	1),
(51,	'admin',	'admin\n',	'2020-10-08 09:37:58',	1);

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `price` float NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `products` (`id`, `name`, `content`, `price`, `description`) VALUES
(1,	'Product1',	'Rjdsjlfkds',	22,	'Perex perex perex'),
(3,	'fgdnjkghfkjd',	'fdsfdsfa',	333.444,	'hfkjdhskjhf'),
(4,	'SuperDuper product',	'Popis a obsah v jednom',	23.45,	'superProduct'),
(5,	'Pridany',	'fdkjgkfjdsg',	222.4,	'mfkldjg'),
(6,	'Produkt2343',	'fdsfdsf',	123.54,	'rfdsfd sfjk djskaf jkdsa');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `role` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `login`, `fullname`, `password`, `role`) VALUES
(1,	'admin',	'Administrator',	'$2y$12$y2s0EIO9QQKzukKy4ZzBa.r1d5XJU2p79MsnN9vejSLt1.68UOGLq',	'admin'),
(2,	'marcel',	'Marcel Vrana',	'$2y$12$Q7MMm628S4O1Xugx6xPkN.cZG2LBH8fNKA9OBz3AdiCn/H054ipgi',	'creator');

-- 2021-06-04 20:31:34
