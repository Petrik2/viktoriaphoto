<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/@layout.latte */
final class Template047d2bef23 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="sk">
<head>
	<title>Viktória Photography</title>
	<meta charset="UTF-8">


	<meta name="wiewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">

	<meta name="description" content="Viktória Lénartová - fotografka">
	<meta name="author" content="Peter Kisel">
	<meta name="robots" content="index, follow">
	<meta name="keywords" content="fotograf,svadobny fotograf, svadba, nevesta, svadobne fotky">
	<link rel="shortcut icon" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 14 */;
		echo '/img/misc/favicon.ico">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous">
	<link rel="stylesheet" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 17 */;
		echo '/css/main.css" type="text/css">

	<script>
            App = {
                basePath: ';
		echo LR\Filters::escapeJs($basePath) /* line 21 */;
		echo ',
            };
	</script>
</head>

<body class="';
		if ($presenter->isLinkCurrent('Homepage:default')) /* line 26 */ {
			echo 'home';
		}
		echo '">
<header class="head-container">

	<a id="logo" href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("homepage:default")) /* line 29 */;
		echo '">Viktória photograpy logo</a>

	<nav class="menu">
		<ul class="primary">
			<li class="list-item" id="bars"><a href=""><i class="fas fa-bars"></i></a></li>
			<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Homepage:default")) /* line 34 */;
		echo '">Úvod</a></li>
			<li class="list-item">
				<a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Services:default")) /* line 36 */;
		echo '">Služby</a>
				<ul class="sub">
					<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Services:default#weddings")) /* line 38 */;
		echo '">Svadby</a></li>
					<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Services:default#family")) /* line 39 */;
		echo '">Rodina a deti</a></li>
					<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Services:default#portraits")) /* line 40 */;
		echo '">Portréty</a></li>
					<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Services:default#product_photo")) /* line 41 */;
		echo '">Produktové a firemné</a></li>
				</ul>
			</li>
			<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Blog:default")) /* line 44 */;
		echo '">Blog</a></li>
			<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default")) /* line 45 */;
		echo '">Portfólio</a></li>
			<li class="list-item"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Contact:default")) /* line 46 */;
		echo '">Kontakt</a></li>
		</ul>
		<div class="lang-select">
			<a href="">SK</a>
			<a href="">HU</a>
		</div>
	</nav>

</header>

';
		$this->renderBlock($ʟ_nm = 'content', [], 'html') /* line 56 */;
		echo '
<footer class="footer-container">
	<div class="left-container">
		<div class="footer-info">
			<i class="fas fa-info"></i>
		</div>
		<div class="bubble-container">
			<div class="speech-bubble">
				<p>
					Stránku vytvoril Peter Kisel
					<br>
					<a href = "mailto: peter.kisel2@gmail.com">peter.kisel2@gmail.com</a>
				</p>
			</div>
		</div>
	</div>
	<div class="footer-contacts">
		<div class="wrapper-left">
			<p> Viktória Lénártová
				<br>
				Dolný Chotár (okres Galanta)
			</p>
		</div>

		<div class="wrapper-right">
			<p>
				<a class="bold" href = "mailto: lenartovaviktoria@gmail.com"><i class="fas fa-envelope"></i> lenartovaviktoria@gmail.com</a>
				<br>
				&copy 2020 Viktória Lénártová
			</p>
		</div>

	</div>

	<div class="footer-social">
		<a href="https://www.facebook.com/Vikt%C3%B3ria-Photography-107026907340187" target="_blank">
			<i class="fab fa-facebook-square"></i>
		</a>
		<a href="https://www.instagram.com/lenart.viktoria/" target="_blank">
			<i class="fab fa-instagram-square"></i>
		</a>
	</div>

</footer>

<div id="overlay">
	<div class="img-container">

	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>
<script src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 111 */;
		echo '/js/main.js"></script>
</body>

</html>



';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}

}
