<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/@layout.latte */
final class Templateacac5393a7 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		'snippet' => ['' => 'block1', 'flashess' => 'blockFlashess'],
	];


	public function main(): array
	{
		extract($this->params);
		echo '



<!doctype html>
<html lang="sk_SK">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


    <title>';
		if ($this->hasBlock("title")) /* line 17 */ {
			$this->renderBlock($ʟ_nm = 'title', [], function ($s, $type) {
				$ʟ_fi = new LR\FilterInfo($type);
				return LR\Filters::convertTo($ʟ_fi, 'html', $this->filters->filterContent('striphtml', $ʟ_fi, $s));
			}) /* line 17 */;
			echo ' | ';
		}
		echo 'Nette Web</title>
  </head>
  <body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 21 */;
		echo '/">Navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Homepage:")) /* line 28 */;
		echo '">Články</a></li>
				<li class="nav-item"><a class="nav-link" href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Event:")) /* line 29 */;
		echo '">Event</a></li>
				<li class="nav-item"><a class="nav-link" href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Products:")) /* line 30 */;
		echo '">Produkty</a></li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
';
		if ($user->isLoggedIn()) /* line 33 */ {
			echo '						Prihlásený ';
			echo LR\Filters::escapeHtmlText($user->identity->fullname) /* line 34 */;
			echo '

					';
		}
		else /* line 36 */ {
			echo 'Neprihlásený';
		}
		echo '
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
';
		if ($user->isLoggedIn()) /* line 39 */ {
			echo '						<a class="nav-link" href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Sign:out")) /* line 40 */;
			echo '">Odhlásit</a>
';
		}
		else /* line 41 */ {
			echo '						<a class="nav-link text-link" href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Sign:in")) /* line 42 */;
			echo '">Prihlásiť sa</a>
						<a class="nav-link text-link" href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Sign:up")) /* line 43 */;
			echo '">Registrovať sa</a>
';
		}
		echo '					</div>
				</li>
				</ul>
		</div>

';
		$this->renderBlock('', [], null, 'snippet') /* line 50 */;
		echo '
		</nav>
  

	<div class="container">
<div id="';
		echo htmlspecialchars($this->global->snippetDriver->getHtmlId('flashess'));
		echo '">';
		$this->renderBlock('flashess', [], null, 'snippet') /* line 57 */;
		echo '</div>
';
		$this->renderBlock($ʟ_nm = 'content', [], 'html') /* line 60 */;
		echo '	</div>

	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
	<script src="https://nette.github.io/resources/js/3/netteForms.min.js"></script>

	<script src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 72 */;
		echo '/js/naja/Naja.min.js"></script>	
	<script src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 73 */;
		echo '/js/script.js"></script>
	


	';
		if ($this->hasBlock("script")) /* line 77 */ {
			$this->renderBlock($ʟ_nm = 'script', [], 'html') /* line 77 */;
		}
		echo '
  </body>
</html>';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['flash' => '58'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {snippetArea } on line 50 */
	public function block1(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		$this->global->snippetDriver->enter('', 'area');
		try {
			/* line 51 */ $_tmp = $this->global->uiControl->getComponent("basketWidget");
			if ($_tmp instanceof Nette\Application\UI\Renderable) $_tmp->redrawControl(null, false);
			$_tmp->render();
		}
		finally {
			$this->global->snippetDriver->leave();
		}
		
	}


	/** {snippet flashess} on line 57 */
	public function blockFlashess(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		$this->global->snippetDriver->enter("flashess", 'static');
		try {
			$iterations = 0;
			foreach ($flashes as $flash) /* line 58 */ {
				echo '		<div';
				echo ($ʟ_tmp = array_filter(['alert', 'alert-' . $flash->type])) ? ' class="' . LR\Filters::escapeHtmlAttr(implode(" ", array_unique($ʟ_tmp))) . '"' : "" /* line 58 */;
				echo '>';
				echo LR\Filters::escapeHtmlText($flash->message) /* line 58 */;
				echo '</div>
';
				$iterations++;
			}
		}
		finally {
			$this->global->snippetDriver->leave();
		}
		
	}

}
