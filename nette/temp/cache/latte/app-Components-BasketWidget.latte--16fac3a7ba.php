<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Components/BasketWidget.latte */
final class Template16fac3a7ba extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		'snippet' => ['' => 'block1'],
	];


	public function main(): array
	{
		extract($this->params);
		echo '<div class="basketWidget" ';
		echo ' id="' . htmlspecialchars($this->global->snippetDriver->getHtmlId('')) . '"';
		echo '>
';
		$this->renderBlock('', [], null, 'snippet');
		echo '</div>';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {snippet } on line 1 */
	public function block1(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		$this->global->snippetDriver->enter("", 'static');
		try {
			echo '	<a href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Basket:")) /* line 2 */;
			echo '" class="badge">
		<span class="basketWidget-itemsCount">V košíku je ';
			echo LR\Filters::escapeHtmlText($itemsCount) /* line 3 */;
			echo ' produktov za ';
			echo LR\Filters::escapeHtmlText($totalPrice) /* line 3 */;
			echo ' &euro;</span>
	</a>
';
		}
		finally {
			$this->global->snippetDriver->leave();
		}
		
	}

}
