<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Basket/default.latte */
final class Template3c39676311 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		0 => ['content' => 'blockContent', 'title' => 'blockTitle'],
		'snippet' => ['productsnip' => 'blockProductsnip'],
	];


	public function main(): array
	{
		extract($this->params);
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 1 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['post' => '33'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 1 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo "\n";
		$this->renderBlock('title', get_defined_vars()) /* line 3 */;
		echo '

<div class="wrapper mt-4 mb-4"';
		echo ' id="' . htmlspecialchars($this->global->snippetDriver->getHtmlId('productsnip')) . '"';
		echo '>
';
		$this->renderBlock('productsnip', [], null, 'snippet');
		echo '</div>
';
		
	}


	/** {block title} on line 3 */
	public function blockTitle(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo '<h1>Nákupný košík</h1>
';
	}


	/** {snippet productsnip} on line 6 */
	public function blockProductsnip(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		$this->global->snippetDriver->enter("productsnip", 'static');
		try {
			if (count($basketProducts) < 1) /* line 7 */ {
				echo '
        <h2>Košík je momentálne prázdny</h2>

';
			}
			else /* line 10 */ {
				echo '    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>
                        Názov
                    </th>
                    <th>
                        Cena
                    </th>
                    <th>
                        Pocet kusov
                    </th>
                    <th>
                        Cena celkom
                    </th>
                    <th>
                    
                    </th>
                </tr>
            </thead>
            <tbody>
';
				$iterations = 0;
				foreach ($basketProducts as $post) /* line 33 */ {
					echo '                    <tr>
                        <td><a href="';
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Products:edit", [$post->id])) /* line 35 */;
					echo '">';
					echo LR\Filters::escapeHtmlText($post->name) /* line 35 */;
					echo '</a></td>
                        <td>';
					echo LR\Filters::escapeHtmlText($post->price) /* line 36 */;
					echo ' &euro;</td>
                        <td><a class="ajax badge badge-info" href="';
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("decrease!", [$post->id])) /* line 37 */;
					echo '">-</a> ';
					echo LR\Filters::escapeHtmlText($post->amount) /* line 37 */;
					echo ' <a class="ajax badge badge-info" href="';
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("increase!", [$post->id])) /* line 37 */;
					echo '">+</a></td>
                        <td>
                            ';
					echo LR\Filters::escapeHtmlText($post->price * $post->amount) /* line 39 */;
					echo ' &euro;
                        </td>
                        <td>
                            <a class="ajax btn btn-danger" href="';
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("delete!", [$post->id])) /* line 42 */;
					echo '">Zmazať</a>
                        </td>
                    </tr>
';
					$iterations++;
				}
				echo '            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5" class="text-right">
                        <span>Celkovo produktov ';
				echo LR\Filters::escapeHtmlText($itemsCount) /* line 50 */;
				echo ' ks za <strong>';
				echo LR\Filters::escapeHtmlText($totalPrice) /* line 50 */;
				echo ' &euro;</strong></span>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
';
			}
		}
		finally {
			$this->global->snippetDriver->leave();
		}
		
	}

}
