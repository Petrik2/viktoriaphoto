<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Blog/default.latte */
final class Template052fffacbe extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['content' => 'blockContent'],
	];


	public function main(): array
	{
		extract($this->params);
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 1 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 1 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '
    <main class="blog">
        <div class="post-container">
            <div class="blog-post 1">

                <div class="blog-image image">
                    <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 8 */;
		echo '/img/photo_upload/portrety1.jpg">
                        <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 9 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
                    </a>
                </div>

                <div class="blog-perex">
                    <h4>Blog post No. 1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta minus
                        vero dignissimos maxime similique labore, exercitationem ratione nam obcaecati
                    </p>
                    <a class="blog-button" href="">Čítaj daľej</a>
                </div>
            </div>

            <div class="blog-post 1">

                <div class="blog-image image">
                    <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 25 */;
		echo '/img/photo_upload/portrety1.jpg">
                        <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 26 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
                    </a>
                </div>

                <div class="blog-perex">
                    <h4>Blog post No. 1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta minus
                        vero dignissimos maxime similique labore, exercitationem ratione nam obcaecati
                    </p>
                    <a class="blog-button" href="">Čítaj daľej</a>
                </div>
            </div>

            <div class="blog-post 1">

                <div class="blog-image image">
                    <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 42 */;
		echo '/img/photo_upload/portrety1.jpg">
                        <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 43 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
                    </a>
                </div>

                <div class="blog-perex">
                    <h4>Blog post No. 1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta minus
                        vero dignissimos maxime similique labore, exercitationem ratione nam obcaecati
                    </p>
                    <a class="blog-button" href="">Čítaj daľej</a>
                </div>
            </div>

            <div class="blog-post 1">

                <div class="blog-image image">
                    <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 59 */;
		echo '/img/photo_upload/portrety1.jpg">
                        <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 60 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
                    </a>
                </div>

                <div class="blog-perex">
                    <h4>Blog post No. 1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta minus
                        vero dignissimos maxime similique labore, exercitationem ratione nam obcaecati
                    </p>
                    <a class="blog-button" href="">Čítaj daľej</a>
                </div>
            </div>

            <div class="blog-post 1">

                <div class="blog-image image">
                    <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 76 */;
		echo '/img/photo_upload/portrety1.jpg">
                        <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 77 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
                    </a>
                </div>

                <div class="blog-perex">
                    <h4>Blog post No. 1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta minus
                        vero dignissimos maxime similique labore, exercitationem ratione nam obcaecati
                    </p>
                    <a class="blog-button" href="">Čítaj daľej</a>
                </div>
            </div>

            <div class="blog-post 1">

                <div class="blog-image image">
                    <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 93 */;
		echo '/img/photo_upload/portrety1.jpg">
                        <img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 94 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
                    </a>
                </div>

                <div class="blog-perex">
                    <h4>Blog post No. 1</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta minus
                        vero dignissimos maxime similique labore, exercitationem ratione nam obcaecati
                    </p>
                    <a class="blog-button" href="">Čítaj daľej</a>
                </div>
            </div>
        </div>
    </main>

';
	}

}
