<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Homepage/default.latte */
final class Template07645fbddc extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		0 => ['content' => 'blockContent', 'title' => 'blockTitle'],
		'snippet' => ['homesnip' => 'blockHomesnip'],
	];


	public function main(): array
	{
		extract($this->params);
		echo "\n";
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 2 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['post' => '9'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 2 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo "\n";
		$this->renderBlock('title', get_defined_vars()) /* line 4 */;
		echo "\n";
		if ($user->isLoggedIn()) /* line 6 */ {
			echo '<a class="btn btn-info" href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Post:create")) /* line 6 */;
			echo '">Vytvořit příspěvek</a>
';
		}
		echo '
<div class="wrapper"';
		echo ' id="' . htmlspecialchars($this->global->snippetDriver->getHtmlId('homesnip')) . '"';
		echo '>
';
		$this->renderBlock('homesnip', [], null, 'snippet');
		echo '</div>';
		
	}


	/** {block title} on line 4 */
	public function blockTitle(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo '<h1>SuperBlog</h1>
';
	}


	/** {snippet homesnip} on line 8 */
	public function blockHomesnip(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		$this->global->snippetDriver->enter("homesnip", 'static');
		try {
			echo '	';
			$iterations = 0;
			foreach ($posts as $post) /* line 9 */ {
				echo '

	<div class="post card mt-4 mb-4">
		<div class="card-header">
			<h2><a href="';
				echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Post:show", [$post->id])) /* line 13 */;
				echo '">';
				echo LR\Filters::escapeHtmlText($post->title) /* line 13 */;
				echo '</a></h2>
		</div>
		<div class="card-body">
			<div>';
				echo LR\Filters::escapeHtmlText(($this->filters->truncate)($post->content, 200)) /* line 16 */;
				echo '</div>
		</div>
		<div class="date card-footer">';
				echo LR\Filters::escapeHtmlText(($this->filters->date)($post->created_at, 'F j, Y')) /* line 18 */;
				echo ' od ';
				echo LR\Filters::escapeHtmlText($post->user['fullname']) /* line 18 */;
				echo "\n";
				if ($user->loggedIn && (($user->id == $post->user['id']) || $user->isInRole('admin'))) /* line 19 */ {
					echo '				<a class="ajax btn btn-warning" href="';
					echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("delete!", [$post->id])) /* line 20 */;
					echo '">Zmazať</a>
';
				}
				echo '		</div>


		
	</div>
';
				$iterations++;
			}
		}
		finally {
			$this->global->snippetDriver->leave();
		}
		
	}

}
