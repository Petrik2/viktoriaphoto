<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Portfolio/default.latte */
final class Template2c2241765f extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['content' => 'blockContent'],
	];


	public function main(): array
	{
		extract($this->params);
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 1 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 1 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '
<main class="portfolio-main">

        <div class="portfolio-list-wrapper">
            <ul class="portfolio-list">
                <li class="selected"><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_1")) /* line 7 */;
		echo '">Album 1</a></li>
                <li><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_2")) /* line 8 */;
		echo '">Album 2</a></li>
                <li><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_3")) /* line 9 */;
		echo '">Album 3</a></li>
                <li><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_4")) /* line 10 */;
		echo '">Album 4</a></li>
                <li><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_5")) /* line 11 */;
		echo '">Album 5</a></li>
                <li><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_6")) /* line 12 */;
		echo '">Album 6</a></li>
                <li><a href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Portfolio:default#Album_7")) /* line 13 */;
		echo '">Album 7</a></li>
            </ul>
        </div>

        <div class="gallery-set image" id="Album_1">
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 18 */;
		echo '/img/photo_upload/svadba_5_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 18 */;
		echo '/img/photo_upload/svadba_5_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 19 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 19 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 20 */;
		echo '/img/photo_upload/svadba_6_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 20 */;
		echo '/img/photo_upload/svadba_6_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 21 */;
		echo '/img/photo_upload/svadba_1_1200px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 21 */;
		echo '/img/photo_upload/svadba_1_1200px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 22 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 22 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 23 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 23 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 24 */;
		echo '/img/photo_upload/portrety1.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 24 */;
		echo '/img/photo_upload/portrety1.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 25 */;
		echo '/img/photo_upload/rodinne3.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 25 */;
		echo '/img/photo_upload/rodinne3.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 26 */;
		echo '/img/photo_upload/portrety2.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 26 */;
		echo '/img/photo_upload/portrety2.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 27 */;
		echo '/img/photo_upload/rodinne2.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 27 */;
		echo '/img/photo_upload/rodinne2.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 28 */;
		echo '/img/photo_upload/svadby2.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 28 */;
		echo '/img/photo_upload/svadby2.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 29 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 29 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 30 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 30 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 31 */;
		echo '/img/photo_upload/portrety1.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 31 */;
		echo '/img/photo_upload/portrety1.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 32 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 32 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 33 */;
		echo '/img/photo_upload/portrety1.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 33 */;
		echo '/img/photo_upload/portrety1.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 34 */;
		echo '/img/photo_upload/rodinne3.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 34 */;
		echo '/img/photo_upload/rodinne3.jpg" alt=""></a>
        </div>

        <div class="gallery-set image" id="Album_2">
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 38 */;
		echo '/img/photo_upload/rodinne2.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 38 */;
		echo '/img/photo_upload/rodinne2.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 39 */;
		echo '/img/photo_upload/svadby2.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 39 */;
		echo '/img/photo_upload/svadby2.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 40 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 40 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 41 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 41 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 42 */;
		echo '/img/photo_upload/portrety1.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 42 */;
		echo '/img/photo_upload/portrety1.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 43 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 43 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
        </div>

        <div class="gallery-set image" id="Album_3">
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 47 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 47 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 48 */;
		echo '/img/photo_upload/portrety1.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 48 */;
		echo '/img/photo_upload/portrety1.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 49 */;
		echo '/img/photo_upload/rodinne3.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 49 */;
		echo '/img/photo_upload/rodinne3.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 50 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 50 */;
		echo '/img/photo_upload/svadba_3_1200px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 51 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 51 */;
		echo '/img/photo_upload/svadba_4_2500px.jpg" alt=""></a>
            <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 52 */;
		echo '/img/photo_upload/portrety1.jpg"><img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 52 */;
		echo '/img/photo_upload/portrety1.jpg" alt=""></a>
        </div>
</main>

';
	}

}
