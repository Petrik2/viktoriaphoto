<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Post/show.latte */
final class Template327f806182 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['content' => 'blockContent', 'title' => 'blockTitle'],
	];


	public function main(): array
	{
		extract($this->params);
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 1 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['comment' => '37'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 1 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo '

<p><a class="btn btn-primary" href="';
		echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Homepage:default")) /* line 4 */;
		echo '">← zpět na výpis příspěvků</a></p>

<div class="card mt-2 mb-4">
    <div class="card-header">
';
		$this->renderBlock('title', get_defined_vars()) /* line 8 */;
		echo '        <div class="date small">';
		echo LR\Filters::escapeHtmlText(($this->filters->date)($post->created_at, 'm.d.Y')) /* line 9 */;
		echo ' napisal ';
		echo LR\Filters::escapeHtmlText($post->user['fullname']) /* line 9 */;
		echo '</div>
    </div>

    <div class="card-body">
        <div class="post">';
		echo LR\Filters::escapeHtmlText($post->content) /* line 13 */;
		echo '</div>
    </div>
</div>









';
		if ($user->loggedIn && ( $user->id == $post->user['id'] || $user->isInRole('admin') )) /* line 25 */ {
			echo '    <a class="btn btn-warning" href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("edit", [$post->id])) /* line 26 */;
			echo '">Upravit příspěvek</a>
';
		}
		echo '
<hr>
<h2>Vložte nový komentár</h2>

';
		/* line 32 */ $_tmp = $this->global->uiControl->getComponent("commentForm");
		if ($_tmp instanceof Nette\Application\UI\Renderable) $_tmp->redrawControl(null, false);
		$_tmp->render();
		echo '
<h2>Komentáře</h2>

<div class="comments">
';
		$iterations = 0;
		foreach ($comments as $comment) /* line 37 */ {
			echo '		<p><b>';
			if ($ʟ_if[3] = ($comment->email)) /* line 38 */ {
				echo '<a href="mailto:';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($comment->email)) /* line 38 */;
				echo '">';
			}
			echo LR\Filters::escapeHtmlText($comment->name) /* line 38 */;
			if ($ʟ_if[3]) /* line 38 */ {
				echo '</a>';
			}
			echo '</b> napsal:</p>
		<div>';
			echo LR\Filters::escapeHtmlText($comment->content) /* line 39 */;
			echo '</div>
';
			$iterations++;
		}
		echo '</div>';
	}


	/** {block title} on line 8 */
	public function blockTitle(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo '        <h1>';
		echo LR\Filters::escapeHtmlText($post->title) /* line 8 */;
		echo '</h1>
';
	}

}
