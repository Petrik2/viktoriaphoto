<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Products/default.latte */
final class Template71e0228eb2 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['content' => 'blockContent', 'title' => 'blockTitle'],
	];


	public function main(): array
	{
		extract($this->params);
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 1 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['post' => '13'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 1 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo "\n";
		$this->renderBlock('title', get_defined_vars()) /* line 3 */;
		echo "\n";
		if ($user->isLoggedIn() && $user->isInRole('admin')) /* line 5 */ {
			echo '<a class="btn btn-info" href="';
			echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Products:list")) /* line 5 */;
			echo '">Administrovať produkty</a>
';
		}
		echo '
<div class="wrapper row">
    
';
		if (count($products) < 1) /* line 9 */ {
			echo '        <h2>Eshop je momentalne prazdny</h2>

';
		}
		else /* line 12 */ {
			$iterations = 0;
			foreach ($products as $post) /* line 13 */ {
				echo '            <div class="col-md-4 col-6">
                <div class="post card mt-4 mb-4">
                    <div class="card-header">
                        <h2><a href="';
				echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("Products:show", [$post->id])) /* line 17 */;
				echo '">';
				echo LR\Filters::escapeHtmlText($post->name) /* line 17 */;
				echo '</a></h2>
                    </div>
                    <div class="card-body">
                        <div>';
				echo LR\Filters::escapeHtmlText($post->description) /* line 20 */;
				echo '</div>
                    </div>
                    <div class="card-footer">
                        <p>Cena: ';
				echo LR\Filters::escapeHtmlText($post->price) /* line 23 */;
				echo ' &euro;</p>
                        <a class="ajax btn btn-success" href="';
				echo LR\Filters::escapeHtmlAttr($this->global->uiControl->link("addToBasket!", [$post->id])) /* line 24 */;
				echo '">Pridať do košíka</a>
                    </div>
                </div>
            </div>
';
				$iterations++;
			}
		}
		echo '	
</div>
';
	}


	/** {block title} on line 3 */
	public function blockTitle(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		echo '<h1>Eshop</h1>
';
	}

}
