<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\Viktoria_photo\nette\app\Presenters/templates/Services/default.latte */
final class Template434922ef0a extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['content' => 'blockContent'],
	];


	public function main(): array
	{
		extract($this->params);
		echo "\n";
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('content', get_defined_vars()) /* line 2 */;
		echo '

';
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	/** {block content} on line 2 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '	<main class="services-main">
		<h3 id="weddings">Služby</h3>
		<div class="wrapper">

			<div class="image item-1">
				<a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 8 */;
		echo '/img/photo_upload/svadba_2_2500px.jpg">
					<img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 9 */;
		echo '/img/photo_upload/svadba_2_2500px.jpg" alt="">
				</a>
			</div>

			<div  class="text item-2">
				<h2>Svadby</h2>
				<br>
				<h5>Svadobné balíky:</h5>
				<br>
				<ul>
					<li>MINIMAL: 150 € / 3h (min. 100 fotografií)</li>
					<li>CLASSIC: 330 € / 8 h (min. 300 fotografií)</li>
					<li>DELUXE: 450 € / 14 h (min. 500 fotografií)</li>
				</ul>
				<br>
				<h5>Ďalšie služby:</h5>
				<br>
				<ul>
					<li><b>Predsvadobné "rande" fotenie:</b> Skúšanie pózovania v pohodlnom oblečení niekoľko mesiacov pred veľkým dňom. Tieto fotky sa dajú použiť na svadobné oznámenia, ako prekvapenie pre svadobných hostí, alebo na dekoráciu svadby. 20 ks / 49 € (trvanie fotenia cca 1 hodinu)</li>
					<li id="family"><b>Handmade album "scrapbook":</b> 19 €</li>
				</ul>
			</div>

			<div  class="text item-3">
				<h2>Rodina a deti</h2>
				<br>
				<p><b>5 ks / 19 € v elektronickej podobe - každá ďalšia fotka 2,90 €</b></p>
				<ul>
					<li>v prípade štúdiového fotenia sa k cene pripočíta 15 € za prenájom štúdia</li>
					<li>možnosť fotenia aj u Vás doma</li>
					<li>v online galérií si sami môžete vybrať fotografie</li>
					<li id="portraits">fotografie posielam vo farebnej aj čierno-bielej verzii</li>
				</ul>
			</div>

			<div class="image item-4">
				<a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 45 */;
		echo '/img/photo_upload/rodinne3.jpg">
					<img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 46 */;
		echo '/img/photo_upload/rodinne3.jpg" alt="">
				</a>
			</div>

			<div class="image item-5">
				<a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 51 */;
		echo '/img/photo_upload/portrety1.jpg">
					<img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 52 */;
		echo '/img/photo_upload/portrety1.jpg" alt="">
				</a>
			</div>

			<div class="text item-6">
				<h2 id="product_photo">Portréty</h2>
				<br>
				<p><b>5 ks / 19 € v elektronickej podobe - každá ďalšia fotka 2,90 €</b></p>
				<ul>
					<li>v prípade štúdiového fotenia sa k cene pripočíta 15 € za prenájom štúdia</li>
					<li>v online galérií si sami môžete vybrať fotografie</li>
					<li >fotografie posielam vo farebnej aj čierno-bielej verzii</li>
				</ul>
			</div>

			<div class="text item-7">
				<h2>Produktové a firemné fotografie</h2>
				<br>
				<p>Od 49 € - cena závisí of miesta fotenia, dĺžky fotenia a samotného produktu. Profesionálne fotky Vám pomôžu osloviť potenciálnych zákazníkov.</p>
			</div>

			<div class="image item-8">
				<a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 74 */;
		echo '/img/photo_upload/produkty1.jpg">
					<img src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 75 */;
		echo '/img/photo_upload/produkty1.jpg" alt="">
				</a>
			</div>
		</div>

	</main>
';
	}

}
