---
name: ❓ Support Question
about: I am stuck and need help with Naja.
labels: support
---

# Support Question

> Please describe your problem in as much detail as possible. Explain what you're trying to achieve
> and include any solutions and code snippets that you have already tried.
